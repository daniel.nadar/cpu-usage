const { spawn } = require('child_process');

// top -l 1 | grep -E "^CPU|^Phys"

const top = spawn('top', ['-l', '1']);
const grep = spawn('grep', ['-E', "^CPU|^Phys"])

top.stdout.on('data', (data) => {
    grep.stdin.write(data)
})

top.stderr.on('data', (data) => {
  console.error(`TOP-OUT ${data}`);
});

top.on('close', (code) => {
  if (code !== 0) {
    console.log(`top process exited with code ${code}`);
  }
  grep.stdin.end();
});

grep.stdout.on('data', (data) => {
  console.log(data.toString());
})

grep.stderr.on('data', (data) => {
  console.error(`grep stderr: ${data}`);
});

grep.on('close', (code) => {
  if (code !== 0) {
    console.log(`grep process exited with code ${code}`);
  }
})



