// ps -A -o %cpu | awk '{s+=$1} END {print s "%"}'

const { spawn } = require('child_process');

const ps = spawn('ps', ['-A', '-o', '%cpu']);
const awk = spawn('awk', ['{s+=$1} END {print s "%"}'])

ps.stdout.on('data', (data) => {
    awk.stdin.write(data)
})

ps.stderr.on('data', (data) => {
  console.error(`top stderr: ${data}`);
});

ps.on('close', (code) => {
  if (code !== 0) {
    console.log(`top process exited with code ${code}`);
  }
  awk.stdin.end();
});

awk.stdout.on('data', (data) => {
  console.log('CPU Usage: ',data.toString());
})

awk.stderr.on('data', (data) => {
  console.error(`grep stderr: ${data}`);
});

awk.on('close', (code) => {
  if (code !== 0) {
    console.log(`grep process exited with code ${code}`);
  }
})



